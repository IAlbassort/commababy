package org.albassort.baby

import java.io.*
import java.lang.Math
import java.time.LocalDateTime.now
import kotlin.math.pow
import org.bukkit.Bukkit
import org.bukkit.ChatColor
import org.bukkit.OfflinePlayer.*
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import org.bukkit.entity.*
import org.bukkit.event.*
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.player.*
import org.bukkit.plugin.java.JavaPlugin

class Comma : JavaPlugin() {
    override fun onEnable() {
        // babycheck does file checking stuff
        filecheck()
        // this register commands from objects
        // multiple need to be loaded and cant be grouped
        getCommand("is")?.setExecutor(IS)
        getCommand("istp")?.setExecutor(ISTP)
        getCommand("isdelete")?.setExecutor(ISDELETE)
        getCommand("isinvitelist")?.setExecutor(ISINVITELIST)
        getCommand("isaccept")?.setExecutor(ISACCEPT)
        getLogger()
                .info(
                        """
                    ${ChatColor.GREEN}
                    ___   ___   ___   ___   ___               ___   ___
                    |   | |       |     | | |     |  /        |       | |
                    |-+-  |-+-    +     + | |-+-  | +         |       +-
                    |  \  |       |     | | |     |/          |       | |
                    |      ---         ---   ---               ---   ---
                    ${ChatColor.BLUE}
                    commababy V.1 8/10/2021"""
                )
        // loads the listening object. There only needs to be one.
        Bukkit.getPluginManager().registerEvents(Init, this)
    }

    override fun onDisable() {}
}

object Init : Listener {
    // listens for players to join.
    var uuidindex: MutableList<MutableList<String>> = ArrayList()
    @EventHandler
    fun onPlayerJoin(event: PlayerJoinEvent) {
        val addition: MutableList<String> = ArrayList()
        // adds player to list when they joins
        val uuid: String = event.getPlayer().getUniqueId().toString()
        addition.add(event.getPlayer().toString())
        addition.add(uuid)
        uuidindex.add(addition)
    }
    // activates when player leaves
    fun onPlayerLeave(event2: PlayerQuitEvent) {
        // pops uuids out when someone leaves
        uuidindex.removeAt(findsublist(uuidindex, event2.getPlayer().toString()))
    }
}

fun invitecheck(sender: CommandSender, reciver: String): Boolean {
    val checkindex: MutableList<List<String>> = getindexblock(File("commababy/invites.txt"))
    val uuid: String = getuuid(sender.toString())
    for (x in checkindex) {
        if (x.containsAll(listOf(uuid, reciver))) {
            return true
        }
    }
    return false
}

object IS : CommandExecutor {
    // does command when /is is done
    override fun onCommand(
            sender: CommandSender,
            cmd: Command,
            lbl: String,
            args: Array<out String>
    ): Boolean {
        val uuid: String = getuuid(sender.toString())
        val cindexblock: MutableList<List<String>> = getindexblock(File("commababy/index.txt"))
        if (args.count() == 0) {
            if (!playerinindex(uuid)) {
                sender.sendMessage("Entry DOESN'T EXIST, making new one :>")
                gennewisland(sender, uuid)
                removeappend(File("commababy/output.txt"))
                return true
            } else {
                babymessage(
                        Message.Pending,
                        " island already exists! do /istp to teleport!",
                        sender
                )
                return false
            }
        } else {
            if (args[0] == "invite") {
                if (args.count() != 2) {
                    babymessage(
                            Message.Error,
                            " Please provide name to send in /is invite <name> format.",
                            sender
                    )
                    return false
                }
                if (!playerinindex(uuid)) {
                    babymessage(Message.Error, " You must have an island to do this...", sender)
                    return false
                } else if (Bukkit.getPlayerExact(args[1]) == null) {
                    babymessage(Message.Error, " Player must be online.", sender)
                    return false
                } else if (invitecheck(sender, args[1])) {
                    babymessage(Message.Error, " You've already sent an invite", sender)
                    return false
                } else {
                    Bukkit.getPlayer(args[1])!!.sendMessage(
                            "${ChatColor.GREEN}[Commababy]:${ChatColor.YELLOW} You've recieved an invite from ${getgoodname(sender)}"
                    )
                    sendinvite(sender, args[1])
                    return true
                }
            }
        }

        if (args[0] == "kick") {
            if (args.count() != 2) {
                babymessage(
                        Message.Error,
                        " Please provide name to send in /is invite <name> format.",
                        sender
                )
                return false
            }
            if (!playerinindex(uuid)) {
                babymessage(Message.Error, " You must have an island to do this...", sender)
                return false
            }
            val innumber: Int = findsublist(cindexblock, uuid)
            if ((cindexblock[innumber].count()) == 2) {
                babymessage(
                        Message.Error,
                        " You're the only person on your island.........",
                        sender
                )
                return false
            } else if (Bukkit.getPlayerExact(args[1]) == null) {
                babymessage(Message.Error, " Player must be online.", sender)
                return false
            } else if (!cindexblock[innumber].contains(getuuid(args[1]))) {
                babymessage(Message.Error, " Player not on island.", sender)
                if (uuid == getuuid(args[1])) {
                    babymessage(Message.Error, " you cant kick yourself lol!.", sender)
                    return false
                }
            } else {
                removeplayerfromisland(getuuid(args[1]), args[1])
                return true
            }
        }

        if (args[0] == "leave") {
            if (!playerinindex(uuid)) {
                babymessage(Message.Error, " You must have an island to do this...", sender)
                return false
            }
            if (isislandowner(uuid)) {
                babymessage(Message.Error, " You cannot leave your own island, silly!", sender)
                return false
            } else {
                removeplayerfromisland(getuuid(sender.toString()), getgoodname(sender))
                return true
            }
        }

        return true
    }
}

object ISTP : CommandExecutor {
    // does command when /is is done
    override fun onCommand(
            sender: CommandSender,
            cmd: Command,
            lbl: String,
            args: Array<out String>
    ): Boolean {
        val sendername: String = getgoodname(sender)
        if (playerinindex(getuuid(sender.toString()))) {
            executeserver(
                    "mvtp ${sendername} e:skyblocks:${(findplayerisland(getuuid(sender.toString())))} "
            )
            return true
        } else {
            babymessage(
                    Message.Error,
                    " You do not have an island! do /is to generate an island!",
                    sender
            )
        }
        return false
    }
}

object ISDELETE : CommandExecutor {
    // does command when /is is done
    override fun onCommand(
            sender: CommandSender,
            cmd: Command,
            lbl: String,
            args: Array<out String>
    ): Boolean {
        val uuid: String = getuuid(sender.toString())
        // if player in index and is owner, remove
        if (playerinindex(uuid)) {
            if (!isislandowner(uuid)) {
                babymessage(
                        Message.Error,
                        " You aren't island owner. do /is leave to leave.",
                        sender
                )
                return false
            }
            removeisland(sender)
            return true
        } else {
            babymessage(
                    Message.Error,
                    " You do not have an island! do /is to generate an island!",
                    sender
            )
        }
        return false
    }
}

object ISACCEPT : CommandExecutor {
    override fun onCommand(
            sender: CommandSender,
            cmd: Command,
            lbl: String,
            args: Array<out String>
    ): Boolean {
        if (args.isEmpty()) {
            babymessage(Message.Error, " PLEASE SPECIFY PLAYER", sender)
            return false
        }
        if (!listinvitesrecived(getuuid(sender.toString())).contains(args[0])) {
            babymessage(
                    Message.Error,
                    " You've yet to recieve an invite from ${args[0]}. do /isinvitelist to see invites",
                    sender
            )
            return false
        }
        if (isislandowner(getuuid(sender.toString()))) {
            babymessage(Message.Error, " you own an island!", sender)
            return false
        }
        if (playerinindex(getuuid(sender.toString()))) {
            babymessage(
                    Message.Error,
                    " you must delete your island before you can be in another's island",
                    sender
            )
            return false
        } else {
            acceptinvite(sender, args[0])
            babymessage(Message.Completed, " invite accepted!", sender)
        }

        return true
    }
}

object ISINVITELIST : CommandExecutor {
    override fun onCommand(
            sender: CommandSender,
            cmd: Command,
            lbl: String,
            args: Array<out String>
    ): Boolean {
        val list: List<String> = listinvitesrecived(getuuid(sender.toString())).toList()
        if (list.isEmpty()) {
            babymessage(Message.Pending, "You've yet to get an invite....", sender)
            return true
        }
        babymessage(Message.Completed, "${list}", sender)
        return true
    }
}

fun gennewisland(sender: CommandSender, uuid: String) {
    if (poslockcheck()) {
        poslock()
        val free = getFreeisland()
        val id = genIslandAddr(getFreeisland())
        bettterwriite(listOf(id, uuid, "#end"), File("commababy/index.txt"))
        // implementation updated
        val idlist: List<String> = sort(id)[1].split(",")
        executeserver("/world skyblocks")
        executeserver("/pos1 -4,59,3")
        executeserver("/pos2 4,70,-3")
        executeserver("/copy")
        executeserver("/pos1 ${idlist[0].toInt()-4},59,${idlist[2].toInt()+3}")
        executeserver("/paste")
        val regionx1: String = sort(id)[2]
        val regionx2: String = sort(id)[3]
        executeserver("/pos1 ${regionx1}")
        executeserver("/pos2 ${regionx2}")
        val sendername: String = getgoodname(sender)
        executeserver("rg define $sendername $sendername")
        babymessage(Message.Pending, " Generating skyblock area!", sender)
        // this allows everything to executed in the correct order
        Thread.sleep(700)
        executeserver("region flag $sendername -g all exit deny")
        executeserver("region flag $sendername -g all exit-via-teleport deny")
        executeserver("region flag $sendername deny-spawn cow,pig,chicken,sheep,horse,donkey")
        executeserver("region flag $sendername deny-spawn cow,pig,chicken,sheep,horse,donkey")
        executeserver("region flag $sendername blocked-cmds sethome")
        babymessage(Message.Completed, " Finished!", sender)
        poslockremove()
        executeserver(
                "mvtp $sendername e:skyblocks:${(findplayerisland(getuuid(sender.toString())))} "
        )
    } else {
        babymessage(
                Message.Pending,
                " another command is being executed. please try again later",
                sender
        )
    }
}

fun removeisland(sender: CommandSender): Boolean {
    val uuid: String = getuuid(sender.toString())
    val newindex: MutableList<List<String>> = getindexblock(File("commababy/index.txt"))
    val itx: Int = findsublist(newindex, uuid)
    if (!poslockcheck()) {
        babymessage(
                Message.Pending,
                " another command is being executed, please try again later",
                sender
        )
        return false
    }
    poslock()
    executeserver("mvtp ${getgoodname(sender)} world")
    Thread.sleep(300)

    Bukkit.getPlayerExact(getgoodname(sender))!!.inventory.clear()

    executeserver("/world skyblocks")
    executeserver("/pos1 ${sort(newindex[itx][0])[4]}")
    executeserver("/pos2 ${sort(newindex[itx][0])[5]}")
    executeserver("/replace air")

    appendfile(newindex[itx][0], File("commababy/output.txt"))
    newindex.removeAt(itx)

    poslockremove()
    // we flatten and write
    write(newindex.flatten().toMutableList(), File("commababy/index.txt"))
    // now we do the rg stuff
    executeserver("rg delete ${getgoodname(sender)}")
    executeserver("delhome ${getgoodname(sender)}:home")
    babymessage(Message.Completed, " Succesfully deleted!", sender)
    return true
}

fun getgoodname(sender: CommandSender): String {
    return sender.toString().split("=")[1].split("}")[0]
}

fun getindexblock(input: File): MutableList<List<String>> {
    val index: MutableList<String> = input.readLines().toMutableList()
    val newindex: MutableList<List<String>> = ArrayList()
    var holder: MutableList<String> = ArrayList()
    newindex.add(listOf(index[0]).toList())
    index.removeAt(0)
    if (index.count() == 1) {
        return newindex
    }
    for (line in index) {
        holder.add(line)
        if (line == "#end") {
            newindex.add(holder.toList())
            holder.clear()
        }
    }
    return newindex
}

fun getFreeisland(): Int {
    /*
    We want to get the lowest possible free island.
    */
    val currentblock = getindexblock(File("commababy/index.txt"))

    if ((currentblock.size) == 1) {
        return 1
    }
    var min = 1
    for (x in currentblock) {
        val islandNumber = sort(x[0])[0].toInt()
        if (islandNumber == min) {
            min++
        }
    }
    return min
}

fun getuuid(sender: String): String {
    val uuidlist: List<List<String>> = Init.uuidindex
    for (x in uuidlist) {

        if (x[0].contains(sender, ignoreCase = true)) {
            return x[1]
        }
    }
    // i don't know why i wrote it like this?
    throw Exception("SOMETHING WEIRD")
}

fun poslock() {
    if (poslockcheck()) {
        File("commababy/pos.lck").createNewFile()
    } else {
        throw Exception("FILE EXISTS")
    }
}

fun poslockremove() {
    if (!poslockcheck()) {
        File("commababy/pos.lck").delete()
    } else {
        throw Exception("FILE DOES NOT EXIST")
    }
}

fun poslockcheck(): Boolean {
    return !File("commababy/pos.lck").exists()
}

enum class Message {
    Pending,
    Completed,
    Error
}

fun babymessage(emotion: Message, input: String, sender: CommandSender) {
    if (emotion == Message.Pending) {
        sender.sendMessage("${ChatColor.GREEN}[Commababy]:${ChatColor.YELLOW}${input}")
        return
    }
    if (emotion == Message.Completed) {
        sender.sendMessage("${ChatColor.GREEN}[Commababy]:${ChatColor.GREEN}${input}")
        return
    }
    if (emotion == Message.Error) {
        sender.sendMessage("${ChatColor.RED}[Commababy]: ${input}")
        return
    }
}

fun executeserver(command: String) {
    Bukkit.dispatchCommand(Bukkit.getServer().getConsoleSender(), command)
}

fun filecheck() {
    val time = java.time.LocalDateTime.now()
    val baby0 = File("commababy/")
    val baby1 = File("commababy/index.txt")
    val exists0: Boolean = baby0.exists()
    if (!exists0) {
        baby0.mkdir()
        baby1.createNewFile()
        baby1.appendText("Header! File created at ${time}")
        if (File("commababy/pos.lck").exists()) {
            File("commababy/pos.lck").delete()
        }
    }
}

fun sort(input: String): List<String> {
    var tween: List<String> = input.split("#")[1].split("!")
    val islandnumber: String = tween[0]
    var islandspawn: String = tween[1].split("/")[0]
    var islandpos1x1: String = tween[1].split("/")[1].split(".")[0]
    var islandpos1x2: String = tween[1].split("/")[1].split(".")[1]
    var islandpos2x1: String = tween[1].split("/")[2].split(".")[0]
    var islandpos2x2: String = tween[1].split("/")[2].split(".")[1]
    var new: List<String> =
            listOf(
                    islandnumber,
                    islandspawn,
                    islandpos1x1,
                    islandpos1x2,
                    islandpos2x1,
                    islandpos2x2
            )
    return new
}

fun appendfile(string: String, file: File) {
    // writes to first line
    val index: MutableList<String> = file.readLines().toMutableList()
    index.add(0, string)
    write(index, file)
}

fun bettterwriite(string: Iterable<String>, file: File) {
    // writes to last line
    val readfile: MutableList<String> = file.readLines().toMutableList()
    for (x in string) {
        readfile.add(x)
    }
    write(readfile, file)
}

fun removeappend(file: File) {
    // removes the firstline
    val index: MutableList<String> = file.readLines().toMutableList()
    index.removeAt(0)
    write(index, file)
}

fun write(input: Iterable<String>, file: File) {
    // rewritesfile
    file.delete()
    val f: RandomAccessFile = RandomAccessFile(file, "rw")
    f.seek(0)
    f.write(input.joinToString("\n").toByteArray())
    f.close()
}

fun findsublist(
        inputlist: Iterable<Iterable<String>>,
        string: String,
        bypassnull: Boolean = false
): Int {
    var it: Int = 0
    for (x in inputlist) {
        if (x.contains(string)) {
            return it
        } else {
            it++
        }
    }
    if (bypassnull) {
        return -1
    }
    throw Exception("STRING NOT FOUND!!")
}

fun playerinindex(uuid: String): Boolean {
    return File("commababy/index.txt").readLines().contains(uuid)
}

fun findplayerisland(uuid: String): String {
    val index: List<List<String>> = getindexblock(File("commababy/index.txt"))
    return sort(index[findsublist(index, uuid)][0])[1]
}

fun isislandowner(uuid: String): Boolean {
    val index: List<List<String>> = getindexblock(File("commababy/index.txt"))
    if (findsublist(index, uuid, bypassnull = true) != -1) {
        return (index[findsublist(index, uuid, bypassnull = true)][1] == uuid)
    }
    return false
}

fun sendinvite(sender: CommandSender, reciver: String) {
    val uuid: String = getuuid(sender.toString())
    val uuid2: String = getuuid(reciver)
    val name: String = getgoodname(sender)
    val writelist: List<String> = listOf(uuid, name, uuid2, reciver, "#end")
    bettterwriite(writelist, File("commababy/invites.txt"))
}

fun listinvitesrecived(uuid: String): List<String> {
    val checkindex: MutableList<List<String>> = getindexblock(File("commababy/invites.txt"))
    var endlist: MutableList<String> = ArrayList()
    // = [x]
    for (x in checkindex) {
        if (uuid in x) {
            if (x[2] == uuid) {
                endlist.add(x[1])
            }
        }
    }
    return endlist
}

fun removeinvite(reciever: CommandSender, sender: String) {
    val uuid: String = getuuid(reciever.toString())
    val checkindex: MutableList<List<String>> = getindexblock(File("commababy/invites.txt"))
    var it: Int = 0
    for (x in checkindex) {
        if (x.containsAll(listOf(uuid, sender))) {
            checkindex.removeAt(it)
            break
        }
        it++
    }
    write(checkindex.flatten(), File("commababy/invites.txt"))
}

fun acceptinvite(reciever: CommandSender, sender: String) {
    val uuid: String = getuuid(reciever.toString())
    val checkindex: MutableList<List<String>> = getindexblock(File("commababy/invites.txt"))
    var newindex: MutableList<List<String>> = getindexblock(File("commababy/index.txt"))
    for (x in checkindex) {
        if (x.containsAll(listOf(uuid, sender))) {
            var newentry: MutableList<String> =
                    newindex[findsublist(newindex, x[0])].toMutableList()
            // grabs the index line with the senders uuid
            newindex.removeAt(findsublist(newindex, x[0]))
            // removes the index from the list so theres no dupes
            newentry.removeAt(newentry.indexOf("#end"))
            // removes end and adds the recievers uuid
            newentry.add(x[2])
            newentry.add("#end")
            newindex.add(newentry)
            write(newindex.flatten(), File("commababy/index.txt"))
            removeinvite(reciever, sender)
            executeserver("/rg addmember -w skyblocks ${x[1]}")
        }
    }
}

fun removeplayerfromisland(uuid: String, name: String) {
    Bukkit.getPlayerExact(name)!!.inventory.clear()
    val cindexblock: MutableList<List<String>> = getindexblock(File("commababy/index.txt"))
    val innumber: Int = findsublist(cindexblock, uuid)
    val block: MutableList<String> = cindexblock[innumber].toMutableList()
    executeserver("/rg removemember -w skyblocks ${name} ${name}")
    cindexblock.removeAt(innumber)
    block.removeAt(block.indexOf(uuid))
    cindexblock.add(block)
    write(cindexblock.flatten(), File("commababy/index.txt"))
    executeserver("mvtp ${name} world")
}
// this was written WAY after the rest of this code.
// I used python to do this before the island and it bugged me, this implementation is also just
// better
fun genIslandAddr(island: Int): String {
    /*
    Magic! you enter the coords and then it pops out where the address works. Its base 1 so islande 1 = 1.
    this only works with this server, but it can work for any server if you apply the inverse of the offset (-4, -5, -3)
    This goes from right to left, drawing islands by lines, originating from the center right corner. ie, -64 -64
    then it gotes to 0 -64, then +64 -64, then -64 0...
    */

    fun cubiciteraotr(a: Int, b: Int): Int {
        /*making this its own function makes the code cleanrer imo */
        var c = a
        var counter = 0
        while (c != b) {
            c += 64
            counter += 1
        }
        return counter
    }

    fun convertToUseableString(input: Array<Int>): String {
        return input.contentToString().replace("[", "").replace("]", "").replace(" ", "")
    }
    var output: MutableList<Array<Int>> = ArrayList()
    var size = 1
    var cube = 9
    // calculates the size of cube and if the island you enter is in it.
    // the equation is ((size*2)+1)^2. or for size 7, 7*7.
    // this is because math has rules and things deriving from the universe.
    while (island > cube) {
        size += 1
        cube = ((size * 2) + 1).toDouble().pow(2.0).toInt()
    }
    var total = 1
    for (cubesize in 1..size) {
        var xstart = 64 * cubesize
        var zstart = -64 * cubesize
        val size = (cubiciteraotr(zstart, xstart))
        var template: MutableList<Int> = ArrayList()
        var currrentint = xstart

        template.add(xstart)
        while (currrentint != zstart) {
            currrentint -= 64
            template.add(currrentint)
        }
        for (x in 0..size) {
            var newline = zstart + (64 * x)

            // so this may look weird, but we do this so we only fully redraw the top and the bottom
            if (Math.abs(newline.toDouble()) == Math.abs(zstart.toDouble())) {
                template.forEach {
                    val array = arrayOf(it, 64, newline)
                    var add = true
                    output.add(array)
                }
                continue
            }
            // and wee add the start and end to the start and end of every other line
            val start = arrayOf(template[0], 64, newline)
            val end = arrayOf(template.last(), 64, newline)
            output.add(start)
            output.add(end)
        }
    }
    /*So, I don't like the fact that I didn't use JSON in the original project, but,
    I don't like the way that the kotlinx serialization does it.
    This entire project has been programed in a very non-oop way, and I don't see the reason to convert it into OOP now.
    If i was restarting, i'd make a class for this. But, this is honestly probably more memory efficient
    than using a custom object.
    */

    val base = output[island - 1]
    val bm1n = convertToUseableString(arrayOf(base[0] + 15, -150, base[2] + 15))
    val bm1p = convertToUseableString(arrayOf(base[0] - 16, 256, base[2] - 16))
    val bm2n = convertToUseableString(arrayOf(base[0] + 31, -150, base[2] + 31))
    val bm2p = convertToUseableString(arrayOf(base[0] - 32, 256, base[2] - 32))
    output.forEach { println(it.contentToString()) }
    return ("#" +
            island +
            "!" +
            convertToUseableString(base) +
            "/" +
            bm1n +
            "." +
            bm1p +
            "/" +
            bm2n +
            "." +
            bm2p)
}
